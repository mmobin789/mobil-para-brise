package com.mobin.mobil.parabrise.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.interfaces.OnHelpClickListener;
import com.mobin.mobil.parabrise.interfaces.OnStepClickListener;

import java.util.List;

/**
 * Created by macbook on 23/11/2017.
 */

public class GeneralViewHolder extends RecyclerView.ViewHolder {
    public TextView day, dayName, jobName, address, jobCreatedAt, jobEndedAt, stepNo, customerName, customerType, jobDesc;
    Button step, help;
    View line, line_bottom;
    private List<?> list;


    GeneralViewHolder(View itemView, final BaseRecyclerAdaper.OnListClickListener listener, AdapterType adapterType) {
        super(itemView);
        setClickListener(listener);

        switch (adapterType) {
            case dates:
                initDateViews();
                break;
            case jobs:
                initJobViews();
                break;
            case steps:
                initStepViews();
                break;
            default:
                initFAQViews();
                break;
        }

    }

    private void initFAQViews() {
        jobName = itemView.findViewById(R.id.question);
        jobDesc = itemView.findViewById(R.id.answer);
    }

    private void initStepViews() {
        step = itemView.findViewById(R.id.name);
        help = itemView.findViewById(R.id.help);
        stepNo = itemView.findViewById(R.id.no);
        line = itemView.findViewById(R.id.line_top);
        line_bottom = itemView.findViewById(R.id.line_bottom);
    }

    void setList(List<?> list) {
        this.list = list;
    }

    private void initDateViews() {
        day = itemView.findViewById(R.id.day);
        dayName = itemView.findViewById(R.id.dayName);
        line = itemView.findViewById(R.id.line);
    }

    private void initJobViews() {
        jobName = itemView.findViewById(R.id.title);
        address = itemView.findViewById(R.id.detail);
        jobCreatedAt = itemView.findViewById(R.id.start);
        jobEndedAt = itemView.findViewById(R.id.end);
        customerName = itemView.findViewById(R.id.customerName);
        customerType = itemView.findViewById(R.id.customerType);
        jobDesc = itemView.findViewById(R.id.desc);
    }

    private void setClickListener(final BaseRecyclerAdaper.OnListClickListener listener) {
        if (listener != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(GeneralViewHolder.this, getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return listener.onItemLongClick(getAdapterPosition());
                }
            });
        }
    }

    void setCustomViewClickListener(View v, BaseRecyclerAdaper.OnListClickListener listener) {
        setClickListener(listener);
        if (listener != null) {

            if (v.getId() == help.getId()) {
                final OnHelpClickListener h = (OnHelpClickListener) listener;
                help.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        h.OnHelpButtonClicked();
                    }
                });
            } else {

                final OnStepClickListener s = (OnStepClickListener) listener;
                step.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int i = getAdapterPosition();
                        s.OnStepClicked(++i, GeneralViewHolder.this);
                    }
                });
            }
        }
    }

    enum AdapterType {
        dates, jobs, steps, faqs
    }
}
