package com.mobin.mobil.parabrise.subcontrollers


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobin.mobil.parabrise.R
import kotlinx.android.synthetic.main.adapter_steps.*


/**
 * A simple [Fragment] subclass.
 * Use the [Step2Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Step2Fragment : BaseFragment() {
    override fun update(data: Bundle) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.adapter_steps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        name.setText(R.string.step2)


        help.visibility = View.VISIBLE
        no.text = "2"

//        view.setOnTouchListener { _, _ ->
//            view.isEnabled = false
//            val s = getString(R.string.step2) + "..."
//            name.text = s
//            getJobDetailActivity().hitStepsApi(2, null)
//            true
//        }
        help.setOnClickListener {
            val data = Bundle()
            data.putInt("step", 2)
            BaseFragment.faq?.update(data)
            getJobDetailActivity().onBackPressed()
        }
        root.getChildAt(1).setOnClickListener(object : View.OnClickListener {


            override fun onClick(p0: View?) {
                val s = getString(R.string.step2) + "..."
                name.text = s
                p0!!.isEnabled = false
                getJobDetailActivity().hitStepsApi(2)


            }
        })
    }
//    private fun getJobDetailActivity(): JobDetailActivity {
//        return activity as JobDetailActivity
//    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Step2Fragment.
         */
        // TODO: Rename and change types and number of parameters
        // TODO: Rename and change types and number of parameters
        fun newInstance(): Step2Fragment {
            val fragment = Step2Fragment()
//            val args = Bundle()
//            args.putString(ARG_PARAM1, step)
//            args.putInt(ARG_PARAM2, position);
            // fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
