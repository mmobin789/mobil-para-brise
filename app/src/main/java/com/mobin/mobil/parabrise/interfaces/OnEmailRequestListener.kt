package com.mobin.mobil.parabrise.interfaces

/**
 * Created by MohammedMobinMunir on 12/24/2017.
 */
interface OnEmailRequestListener {
    fun onForgotPasswordApiResponse(message: String)

}