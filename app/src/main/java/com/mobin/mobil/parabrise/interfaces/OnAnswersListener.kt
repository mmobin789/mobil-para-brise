package com.mobin.mobil.parabrise.interfaces

import com.mobin.mobil.parabrise.model.Answer

/**
 * Created by macbook on 27/12/2017.
 */
interface OnAnswersListener {
    fun onAnswersReceived(answers: MutableList<Answer>)

    fun onListingFailed(error: String)
}