package com.mobin.mobil.parabrise.model

import com.google.gson.annotations.SerializedName

/**
 * Created by macbook on 27/12/2017.
 */
class Question {
    @SerializedName("name")
    val question = ""
    val id = 0
    val count = 0
    val description = ""
    @SerializedName("parent")
    val parentID = 0
    var answer = Answer()

}