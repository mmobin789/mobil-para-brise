//package com.mobin.mobil.parabrise.subcontrollers;
//
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.mobin.mobil.parabrise.R;
//
///**
// * A simple {@link Fragment} subclass.
// * Use the {@link Step1FragmentJava#newInstance} factory method to
// * create an instance of this fragment.
// */
//public class Step1FragmentJava extends BaseFragment {
//    // TODO: Rename parameter arguments, choose names that match
//
//
//    public Step1FragmentJava() {
//        // Required empty public constructor
//    }
//
//
//    // TODO: Rename and change types and number of parameters
//    @NonNull
//    public static Step1FragmentJava newInstance() {
//
//        return new Step1FragmentJava();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        final TextView name = view.findViewById(R.id.name);
//        TextView no = view.findViewById(R.id.no);
//        no.setText("1");
//        Button help = view.findViewById(R.id.help);
//        name.setText(R.string.step1);
//        name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String s = getString(R.string.step1) + "...";
//                name.setText(s);
//                getJobDetailActivity().hitStepsApi(1, null);
//            }
//        });
//    }
//
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.adapter_steps, container, false);
//    }
//
//}
