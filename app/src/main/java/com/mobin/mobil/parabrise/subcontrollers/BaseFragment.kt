package com.mobin.mobil.parabrise.subcontrollers

import android.os.Bundle
import android.support.v4.app.Fragment
import com.mobin.mobil.parabrise.controllers.JobDetailActivity
import com.mobin.mobil.parabrise.controllers.MainActivity

/**
 * Created by macbook on 18/12/2017.
 */
abstract class BaseFragment : Fragment() {

    companion object {
        var faq: FAQFragment? = null
        var home: HomeFragment? = null
        var account: ProfileFragment? = null
        var map: MapFragment? = null
    }


    protected fun getMainActivity(): MainActivity {
        return activity as MainActivity
    }

    protected fun getJobDetailActivity(): JobDetailActivity {
        return activity as JobDetailActivity
    }


    abstract fun update(data: Bundle)
}