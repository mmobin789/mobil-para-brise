package com.mobin.mobil.parabrise.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * Created by macbook on 22/11/2017.
 */

public final class SLeekUI {

    private static List<Bitmap> bitmaps = new ArrayList<>();

    private SLeekUI() {
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @NonNull
    public static String capWordFirstLetter(String fullname) {
        StringBuilder fname = new StringBuilder();
        String s2;
        StringTokenizer tokenizer = new StringTokenizer(fullname);
        while (tokenizer.hasMoreTokens()) {
            s2 = tokenizer.nextToken().toLowerCase();
            if (fname.length() == 0)
                fname.append(s2.substring(0, 1).toUpperCase()).append(s2.substring(1));
            else
                fname.append(" ").append(s2.substring(0, 1).toUpperCase()).append(s2.substring(1));
        }

        return fname.toString();
    }

//    public static String getFrenchCalendarCurrentDate() {
//        Calendar calendar = Calendar.getInstance();
//        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.FRANCE);
//        return serverFormat.format(calendar.getTime());
//    }

    public static String formattedFrenchDate(String dateS)

    {
        String formatted = "";
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.FRANCE);
        SimpleDateFormat requiredFormat = new SimpleDateFormat("k:mm", Locale.FRANCE);
        try {
            Date date = serverFormat.parse(dateS);
            formatted = requiredFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatted;
    }

    public static Bitmap takeScreenShot(View v) {
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);
        Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return bitmap;

    }

    public static void setBitmapInList(Bitmap bitmap) {
        bitmaps.add(bitmap);
    }

    public static Bitmap getBitmapFromList(int index) {
        return bitmaps.get(index);
    }

    @NonNull
    public static String capitalizeWordAt(String word, int index) {
        StringBuilder sb = new StringBuilder(word);
        sb.setCharAt(index, Character.toUpperCase(sb.charAt(index)));
        return sb.toString();
    }

    public static Date getFrenchTimeFromString(String timeStamp) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.FRANCE);
        Date date = null;
        try {
            date = serverFormat.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static String getDayInFrench(String dayInEnglish) {
        String day = "";
        switch (dayInEnglish.toUpperCase()) {
            case "MON":
                day = "Lun";
                break;
            case "TUE":
                day = "Mar";
                break;
            case "WED":
                day = "Mer";
                break;
            case "THU":
                day = "Jeu";
                break;
            case "FRI":
                day = "Ven";
                break;
            case "SAT":
                day = "Sam";
                break;
            case "SUN":
                day = "Dim";
                break;


        }

        return day;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {

        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                px,
                Resources.getSystem().getDisplayMetrics()
        );
    }

    public static String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return String.valueOf(Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY));
        } else {
            return String.valueOf(Html.fromHtml(html));
        }
    }

    public static String formattedMinutes(Context context, String dateS)

    {
        String formatted = "";
        long diff;
        Calendar calendar = Calendar.getInstance();
        Date current = calendar.getTime();
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", getCurrentLocale(context));
        SimpleDateFormat requiredFormat = new SimpleDateFormat("mm", getCurrentLocale(context));
        try {
            Date jobDate = serverFormat.parse(dateS);
            formatted = requiredFormat.format(jobDate);
            diff = jobDate.getTime() - current.getTime();
            formatted = requiredFormat.format(new Date(diff));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatted;
    }

    private static Locale getCurrentLocale(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return c.getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return c.getResources().getConfiguration().locale;
        }
    }

    public static void initRecyclerView(RecyclerView recyclerView, boolean vertical) {
        int i;
        if (vertical)
            i = LinearLayoutManager.VERTICAL;
        else
            i = LinearLayoutManager.HORIZONTAL;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), i, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


    }

    public static void initRecyclerViewNoNScrollable(final RecyclerView recyclerView, boolean vertical) {
        int i;
        if (vertical)
            i = LinearLayoutManager.VERTICAL;
        else
            i = LinearLayoutManager.HORIZONTAL;

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext(), i, false) {
//            @Override
//            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
//                SnappySmoothScroller scroller = new SnappySmoothScroller.Builder()
//                        .setPosition(position)
//                        .setSnapType(SnapType.CENTER)
//                        .setSnapDuration(1000)
//                        .setSnapInterpolator(new DecelerateInterpolator())
//                        .setScrollVectorDetector(new LinearLayoutScrollVectorDetector(this))
//                        .build(recyclerView.getContext());
//
//                startSmoothScroll(scroller);
//            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };


        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


    }

    public static void updateUI(final TextView textView, int newStringID, int labelStringID) {
        final String label = textView.getContext().getString(labelStringID);
        String txt = textView.getContext().getString(newStringID);
        textView.setText(txt.toUpperCase());
        textView.setEnabled(false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                textView.setText(label);
                textView.setEnabled(true);

            }
        }, 5000);
    }

    public static void updateUI(final TextView textView, String txt, int labelStringID) {
        final String label = textView.getContext().getString(labelStringID);
        textView.setText(txt.toUpperCase());
        textView.setEnabled(false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                textView.setText(label);
                textView.setEnabled(true);

            }
        }, 5000);
    }
}
