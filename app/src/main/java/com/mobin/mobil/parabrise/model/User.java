package com.mobin.mobil.parabrise.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by macbook on 22/11/2017.
 */

public class User implements Parcelable {

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
    public static double lat, lng;
    public String token;
    @SerializedName("user_display_name")
    public String name;
    @SerializedName("user_id")
    public int id;
    @SerializedName("user_email")
    public String email;
    @SerializedName("user_roles")
    public List<String> roles;
    @SerializedName("token_expires")
    private double tokenExpiry;
    private String nom, prenom;

    private User(Parcel in) {
        this.tokenExpiry = in.readDouble();
        this.token = in.readString();
        this.nom = in.readString();
        this.prenom = in.readString();
        this.email = in.readString();
        this.name = in.readString();
        this.roles = in.createStringArrayList();
        this.id = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.tokenExpiry);
        dest.writeString(this.token);
        dest.writeString(this.nom);
        dest.writeString(this.prenom);
        dest.writeString(this.email);
        dest.writeString(this.name);
        dest.writeStringList(this.roles);
        dest.writeInt(this.id);
    }
}
