package com.mobin.mobil.parabrise.controllers

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.os.Build
import android.os.Bundle
import android.view.DragEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.OnDragListener
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.utils.SLeekUI
import kotlinx.android.synthetic.main.fullscreen.*


class FullScreenActivity : BaseActivity() {
    override fun initViews() {

    }

    override fun setEvents() {

        op1.setOnTouchListener(MyTouchListener())
        op2.setOnTouchListener(MyTouchListener())
        op3.setOnTouchListener(MyTouchListener())
        // root.setOnDragListener(MyDragListener())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.fullscreen)
        super.onCreate(savedInstanceState)
    }

    private inner class MyDragListener : OnDragListener {

        var pic = 0
        override fun onDrag(v: View, event: DragEvent): Boolean {
            when (event.action) {
                DragEvent.ACTION_DRAG_STARTED -> {
                }
//                DragEvent.ACTION_DRAG_ENTERED -> v.setBackgroundDrawable(enterShape)
//                DragEvent.ACTION_DRAG_EXITED -> v.setBackgroundDrawable(normalShape)
                DragEvent.ACTION_DROP -> {
                    // Dropped, reassign View to ViewGroup
                    // Dropped, reassign View to ViewGroup
                    val view = event.localState as View
                    val owner = view.parent as ViewGroup
                    owner.removeView(view)
                    val container = v as RelativeLayout
                    container.addView(view)
                    view.visibility = View.VISIBLE
                    val screenshot = SLeekUI.takeScreenShot(root)
                    SLeekUI.setBitmapInList(screenshot)
                    setResult(Activity.RESULT_OK)
                    if (pic == 3)
                        onBackPressed()

                    pic++
                }
                DragEvent.ACTION_DRAG_ENDED -> {
                }
                else -> {
                }
            }// do nothing
            return true
        }
    }

    private inner class MyTouchListener : OnTouchListener {
        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                val data = ClipData.newPlainText("", "")
                val shadowBuilder = View.DragShadowBuilder(
                        view)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    view.startDragAndDrop(data, shadowBuilder, view, 0)
                } else {
                    view.startDrag(data, shadowBuilder, view, 0)
                }
                view.visibility = View.INVISIBLE
                return true
            } else {
                return false
            }
        }
    }
}
