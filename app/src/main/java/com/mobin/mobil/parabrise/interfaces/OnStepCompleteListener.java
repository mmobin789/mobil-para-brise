package com.mobin.mobil.parabrise.interfaces;

/**
 * Created by macbook on 27/11/2017.
 */

public interface OnStepCompleteListener {
    void OnStepCompleted(int step);

    void OnStepFailed(int step, String error);
}
