package com.mobin.mobil.parabrise.interfaces

import com.mobin.mobil.parabrise.model.Question

/**
 * Created by macbook on 27/12/2017.
 */
interface OnFAQsListingListener {

    fun onListingReceived(questions: MutableList<Question>)

    fun onListingFailed(error: String)
}