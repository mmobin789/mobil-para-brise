package com.mobin.mobil.parabrise.subcontrollers


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.adapters.JobInfoWindowAdapter
import com.mobin.mobil.parabrise.controllers.BaseActivity
import com.mobin.mobil.parabrise.controllers.JobDetailActivity
import com.mobin.mobil.parabrise.interfaces.OnListingListener
import com.mobin.mobil.parabrise.model.JobMarkerInfo
import com.mobin.mobil.parabrise.model.PlumberJobs
import com.mobin.mobil.parabrise.utils.SLeekUI


/**
 * A simple [Fragment] subclass.
 * Use the [MapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MapFragment : BaseFragment(), OnMapReadyCallback {

    var googleMap: GoogleMap? = null
    override fun update(data: Bundle) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @SuppressLint("MissingPermission")
    fun enableLocation() {
        if (BaseActivity.hasLocationPermission(getMainActivity()))
            googleMap?.isMyLocationEnabled = true
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        this.googleMap = googleMap
        enableLocation()
        BaseFragment.home!!.setOnJobsListener(object : OnListingListener {


            override fun onListingReceived(plumberJobs: MutableList<PlumberJobs>?) {
                if (plumberJobs != null) {
                    for (plumberJob in plumberJobs) {
                        val markerOp = MarkerOptions()
                        markerOp.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                        val lat = plumberJob.extra.lat
                        val lng = plumberJob.extra.lng
                        if (lat != null && lng != null) {
                            val d0 = lat.toDouble()
                            val d1 = lng.toDouble()
                            markerOp.position(LatLng(d0, d1))

                            BaseActivity.zoomToLocation(googleMap, d0, d1)
                            val jobMarkerInfo = JobMarkerInfo()
                            jobMarkerInfo.header = plumberJob.extra.schedule_title
                            jobMarkerInfo.startTime = SLeekUI.formattedFrenchDate(plumberJob.start_date)
                            jobMarkerInfo.endTime = SLeekUI.formattedFrenchDate(plumberJob.end_date)
                            val jobWindow = JobInfoWindowAdapter(context!!, jobMarkerInfo)
                            googleMap?.setInfoWindowAdapter(jobWindow)
                            val marker = googleMap?.addMarker(markerOp)
                            googleMap?.setOnMarkerClickListener {
                                marker?.showInfoWindow()
                                marker!!.isInfoWindowShown
                            }
                            googleMap?.setOnInfoWindowClickListener {

                                val detail = Intent(getMainActivity(), JobDetailActivity::class.java)
                                detail.putExtra("plumberJob", plumberJob)
                                startActivity(detail)
                            }


                        }
                    }
                }
            }

            override fun onListingFailed(error: String?) {

            }


        })


    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getMainActivity().setMapUI(this, this, R.id.map)
    }


    companion object {
        // TODO: Rename parameter arguments, choose names that match


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MapFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(): MapFragment {
            val fragment = MapFragment()
//            val args = Bundle()
//            args.putString(ARG_PARAM1, param1)
//            args.putString(ARG_PARAM2, param2)
//            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
