package com.mobin.mobil.parabrise.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by macbook on 23/11/2017.
 */

public class Contact implements Parcelable {
    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
    private String created_by = "";
    private String phone = "";
    private String other = "";
    private String fax = "";
    private String website = "";
    private String state = "";
    private String currency = "";
    private String country = "";
    private String city = "";
    private String street_2 = "";
    private String id = "";
    private String first_name = "";
    private String street_1 = "";
    private String created = "";
    private String email = "";
    private String postal_code = "";
    private String company = "";
    private String last_name = "";
    private String user_id = "";
    private String[] types = {""};
    private String notes = "";
    private String mobile = "";


    private Contact(Parcel in) {
        this.created_by = in.readString();
        this.phone = in.readString();
        this.other = in.readString();
        this.fax = in.readString();
        this.website = in.readString();
        this.state = in.readString();
        this.currency = in.readString();
        this.country = in.readString();
        this.city = in.readString();
        this.street_2 = in.readString();
        this.id = in.readString();
        this.first_name = in.readString();
        this.street_1 = in.readString();
        this.created = in.readString();
        this.email = in.readString();
        this.postal_code = in.readString();
        this.company = in.readString();
        this.last_name = in.readString();
        this.user_id = in.readString();
        this.types = in.createStringArray();
        this.notes = in.readString();
        this.mobile = in.readString();
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getState() {
        if (TextUtils.isEmpty(state))
            state = "";
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        if (TextUtils.isEmpty(country))
            country = "";
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet_2() {
        return street_2;
    }

    public void setStreet_2(String street_2) {
        this.street_2 = street_2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getStreet_1() {
        return street_1;
    }

    public void setStreet_1(String street_1) {
        this.street_1 = street_1;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public String getNotes() {
        if (TextUtils.isEmpty(notes))
            notes = "";
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "ClassPojo [created_by = " + created_by + ", phone = " + phone + ", other = " + other + ", fax = " + fax + ", website = " + website + ", state = " + state + ", currency = " + currency + ", country = " + country + ", city = " + city + ", street_2 = " + street_2 + ", id = " + id + ", first_name = " + first_name + ", street_1 = " + street_1 + ", created = " + created + ", email = " + email + ", postal_code = " + postal_code + ", company = " + company + ", last_name = " + last_name + ", user_id = " + user_id + ", types = " + types + ", notes = " + notes + ", mobile = " + mobile + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.created_by);
        dest.writeString(this.phone);
        dest.writeString(this.other);
        dest.writeString(this.fax);
        dest.writeString(this.website);
        dest.writeString(this.state);
        dest.writeString(this.currency);
        dest.writeString(this.country);
        dest.writeString(this.city);
        dest.writeString(this.street_2);
        dest.writeString(this.id);
        dest.writeString(this.first_name);
        dest.writeString(this.street_1);
        dest.writeString(this.created);
        dest.writeString(this.email);
        dest.writeString(this.postal_code);
        dest.writeString(this.company);
        dest.writeString(this.last_name);
        dest.writeString(this.user_id);
        dest.writeStringArray(this.types);
        dest.writeString(this.notes);
        dest.writeString(this.mobile);
    }
}
