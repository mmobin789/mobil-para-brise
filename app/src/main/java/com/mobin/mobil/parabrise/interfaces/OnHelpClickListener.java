package com.mobin.mobil.parabrise.interfaces;

import com.mobin.mobil.parabrise.adapters.BaseRecyclerAdaper;

/**
 * Created by macbook on 24/11/2017.
 */

public interface OnHelpClickListener extends BaseRecyclerAdaper.OnListClickListener {
    void OnHelpButtonClicked();
}
