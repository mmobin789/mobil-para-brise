//package com.mobin.mobil.parabrise.subcontrollers;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.mobin.mobil.parabrise.R;
//
///**
// * Created by macbook on 19/12/2017.
// */
//
//public class Step2FragmentJava extends BaseFragment {
//
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//
//    public Step2FragmentJava() {
//        // Required empty public constructor
//    }
//
//
//    // TODO: Rename and change types and number of parameters
//    @NonNull
//    public static Step2FragmentJava newInstance() {
//
//        return new Step2FragmentJava();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        final TextView name = view.findViewById(R.id.name);
//        TextView no = view.findViewById(R.id.no);
//        no.setText("2");
//        Button help = view.findViewById(R.id.help);
//        name.setText(R.string.step2);
//        name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String s = getString(R.string.step2) + "...";
//                name.setText(s);
//                getJobDetailActivity().hitStepsApi(2, null);
//            }
//        });
//    }
//
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.adapter_steps, container, false);
//    }
//
//
//}
