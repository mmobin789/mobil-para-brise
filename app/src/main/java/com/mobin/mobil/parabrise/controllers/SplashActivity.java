package com.mobin.mobil.parabrise.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.crashlytics.android.Crashlytics;
import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.helpers.AppPersistence;

import io.fabric.sdk.android.Fabric;


public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        setContentView(R.layout.splash_screen);


        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setEvents() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                check();
            }
        }, 2000);

    }

    private void check() {
        AppPersistence.checkUser(this);
        Intent according = new Intent();
        //according.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (AppPersistence.userLogged) {
            according.setClass(this, MainActivity.class);
        } else {
            according.setClass(this, LoginActivity.class);
        }
        finish();
        startActivity(according);

    }
}
