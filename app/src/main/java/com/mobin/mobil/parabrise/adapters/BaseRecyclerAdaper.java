package com.mobin.mobil.parabrise.adapters;

import android.support.v7.widget.RecyclerView;

/**
 * Created by macbook on 23/11/2017.
 */

public abstract class BaseRecyclerAdaper extends RecyclerView.Adapter<GeneralViewHolder> {
    OnListClickListener listener;


    public void setOnItemClickListener(OnListClickListener listener) {
        this.listener = listener;
    }


    public interface OnListClickListener {
        void onItemClick(GeneralViewHolder generalViewHolder, int position);

        boolean onItemLongClick(int position);

    }
}
