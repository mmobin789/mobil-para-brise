package com.mobin.mobil.parabrise.api;

import com.mobin.mobil.parabrise.model.Answer;
import com.mobin.mobil.parabrise.model.Auth;
import com.mobin.mobil.parabrise.model.PlumberCalendar;
import com.mobin.mobil.parabrise.model.Question;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by macbook on 22/11/2017.
 */

interface API {
    @POST("simple-jwt-authentication/v1/token")
    Call<Auth> login(@Body Auth auth);


    @GET
    Call<PlumberCalendar> getListing(@Header("Authorization") String header, @Url String url);


    @GET
    Call<ResponseBody> steps(@Header("Authorization") String header, @Url String url);

    @POST
    @FormUrlEncoded
    Call<ResponseBody> stepsImage(@Header("Authorization") String header, @Url String url, @FieldMap HashMap<String, String> params);

    @GET
    Call<ResponseBody> forgotPassword(@Url String url);

    @GET
    Call<ResponseBody> sendLocation(@Url String url);

    @GET
    Call<List<Question>> getFAQs(@Url String url);

    @GET
    Call<List<Answer>> getAnswers(@Url String url);
}
