package com.mobin.mobil.parabrise.controllers

import android.os.Bundle
import android.text.InputType
import android.view.View.GONE
import android.widget.Toast
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.api.APIMobil
import com.mobin.mobil.parabrise.interfaces.OnEmailRequestListener
import com.mobin.mobil.parabrise.utils.SLeekUI
import com.mobin.mobil.parabrise.utils.SLeekUI.updateUI
import kotlinx.android.synthetic.main.activity_login.*

class ForgotPasswordActivity : BaseActivity() {

    private fun recoverPassword(email: String) {


        SLeekUI.updateUI(login, R.string.requesting, R.string.req_password)
        APIMobil.forgotPassword(object : OnEmailRequestListener {

            override fun onForgotPasswordApiResponse(message: String) {
                if (message.contains("Erreur"))
                    updateUI(login, R.string.email_not_found, R.string.req_password)
                else
                    updateUI(login, message.replace("\u00E0", ""), R.string.req_password)
            }


        }, email)
    }

    override fun setEvents() {
        login.setOnClickListener {

            val email = name.text.toString()
            if (SLeekUI.isValidEmail(email)) {
                recoverPassword(email)
            } else
                Toast.makeText(this, R.string.wrong_email, Toast.LENGTH_SHORT).show()
        }
    }

    override fun initViews() {

        name.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        name.hint = getString(R.string.email)

        label1.setImageResource(R.drawable.email)
        input2.visibility = GONE
        fp.visibility = GONE
        login.setText(R.string.req_password)

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_login)
        super.onCreate(savedInstanceState)

    }
}
