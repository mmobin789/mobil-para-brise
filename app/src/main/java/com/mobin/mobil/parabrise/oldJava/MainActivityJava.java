//package com.mobin.mobil.parabrise.controllers;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.view.ViewPager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.mobin.mobil.parabrise.R;
//import com.mobin.mobil.parabrise.adapters.BaseRecyclerAdaper;
//import com.mobin.mobil.parabrise.adapters.DatesAdapter;
//import com.mobin.mobil.parabrise.adapters.JobsAdapter;
//import com.mobin.mobil.parabrise.adapters.TabsAdapter;
//import com.mobin.mobil.parabrise.api.APIMobil;
//import com.mobin.mobil.parabrise.helpers.ExceptionDealer;
//import com.mobin.mobil.parabrise.interfaces.OnListingListener;
//import com.mobin.mobil.parabrise.location.LocationFetcher;
//import com.mobin.mobil.parabrise.model.PlumberCalendar;
//import com.mobin.mobil.parabrise.model.PlumberJobs;
//import com.mobin.mobil.parabrise.utils.SLeekUI;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//import java.util.Locale;
//import java.util.TimeZone;
//
//public class MainActivity extends BaseActivity implements OnListingListener {
//    RecyclerView recyclerView, rv2;
//    int lastCheckedPosition = 1;
//    TextView loadingTV, errorTV;
//    RelativeLayout root;
//    List<PlumberCalendar> list = new ArrayList<>();
//    Calendar currentTimeC;
//    ViewPager pager;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionDealer());
//        setContentView(R.layout.activity_main);
//        super.onCreate(savedInstanceState);
//
//
//    }
//
//
//    @Override
//    protected void initViews() {
////
////        recyclerView = findViewById(R.id.rv);
////        rv2 = findViewById(R.id.rv2);
////        loadingTV = findViewById(R.id.loading);
////        errorTV = findViewById(R.id.error);
//        pager = findViewById(R.id.pager);
////        SLeekUI.initRecyclerView(recyclerView, false);
////        SLeekUI.initRecyclerView(rv2, true);
////        root = findViewById(R.id.root);
////        setDatesAdapter();
//
//
//    }
//
//
//    private void setDatesAdapter() {
//        list.clear();
//        currentTimeC = Calendar.getInstance();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
//        String frenchDate = dateFormat.format(currentTimeC.getTime());
//        getLabelView(R.id.header).setText(SLeekUI.capWordFirstLetter(frenchDate));
//        currentTimeC.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
//        for (int i = 0; i < 7; i++) {
//            if (i == 0)
//                currentTimeC.add(Calendar.DATE, -1);
//            else currentTimeC.add(Calendar.DATE, 1);
//
//            //   String dayOfTheWeek = dayNameFormat.format(currentTimeC.getTime());
//
//            list.add(new PlumberCalendar(currentTimeC.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.FRANCE), currentTimeC.get(Calendar.DAY_OF_MONTH)));
//
//
//        }
//        list.get(lastCheckedPosition).setSelected(true);
//
//        final DatesAdapter adapter = new DatesAdapter(list);
//        recyclerView.setAdapter(adapter);
//
//        adapter.setOnItemClickListener(new BaseRecyclerAdaper.OnListClickListener() {
//            @Override
//            public void onItemClick(int position) {
//
//
//                list.get(lastCheckedPosition).setSelected(false);
//                list.get(position).setSelected(true);
//                lastCheckedPosition = position;
//                adapter.notifyDataSetChanged();
//                hitListingAPI(position);
//            }
//
//            @Override
//            public boolean OnItemLongClick(int position) {
//                return false;
//            }
//        });
//
//
//    }
//
//    private void hitListingAPI(int position) {
//        loadingTV.setVisibility(View.VISIBLE);
//        rv2.setVisibility(View.GONE);
//        errorTV.setVisibility(View.INVISIBLE);
//        APIMobil.getListing(new String[]{currentTimeC.get(Calendar.YEAR) + "", String.valueOf(currentTimeC.get(Calendar.MONTH) + 1) + "", list.get(position).getDay()}, MainActivity.this);
//
//    }
//
//    @Override
//    protected void setEvents() {
//        //  hitListingAPI(lastCheckedPosition);
//        pager.setAdapter(new TabsAdapter(getSupportFragmentManager()));
//        startService(new Intent(this, LocationFetcher.class));
//
//
//    }
//
//
//    @Override
//    public void onListingReceived(List<PlumberJobs> plumberJobs) {
//
//        loadingTV.setVisibility(View.INVISIBLE);
//
//
//        if (plumberJobs != null && plumberJobs.size() > 0) {
//            rv2.setVisibility(View.VISIBLE);
//            updateUI(plumberJobs);
//
//        } else {
//            errorTV.setVisibility(View.VISIBLE);
//            rv2.setVisibility(View.GONE);
//            errorTV.setText(R.string.no_data);
//
//        }
//    }
//
//    private void updateUI(final List<PlumberJobs> plumberJobs) {
//        JobsAdapter adapter = new JobsAdapter(plumberJobs);
//        rv2.setAdapter(adapter);
//
//
//        adapter.setOnItemClickListener(new BaseRecyclerAdaper.OnListClickListener() {
//            @Override
//            public void onItemClick(int position) {
//                PlumberJobs p = plumberJobs.get(position);
//                Intent detail = new Intent(MainActivity.this, JobDetailActivity.class);
//                detail.putExtra("plumberJob", p);
//                startActivity(detail);
//
//
//            }
//
//            @Override
//            public boolean OnItemLongClick(int position) {
//                return false;
//            }
//        });
//
//    }
//
//
//    @Override
//    public void onListingFailed(String error) {
//        loadingTV.setVisibility(View.INVISIBLE);
//        rv2.setVisibility(View.GONE);
//        errorTV.setVisibility(View.VISIBLE);
//        errorTV.setText(R.string.no_data);
//        Log.e("ListingError", error);
//    }
//
//
//}
