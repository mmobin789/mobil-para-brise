package com.mobin.mobil.parabrise.adapters;

import android.view.View;
import android.view.ViewGroup;

import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.model.PlumberCalendar;

import java.util.List;

/**
 * Created by macbook on 22/11/2017.
 */

public class DatesAdapter extends BaseRecyclerAdaper {

    private List<PlumberCalendar> calendar;

    public DatesAdapter(List<PlumberCalendar> calendar) {
        this.calendar = calendar;
    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        GeneralViewHolder holder = new GeneralViewHolder(View.inflate(parent.getContext(), R.layout.adapter_layout, null), listener, GeneralViewHolder.AdapterType.dates);
        holder.setList(calendar);
        return holder;


    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        PlumberCalendar plumberCalendar = calendar.get(position);
        if (plumberCalendar.isSelected()) {
            holder.line.setVisibility(View.VISIBLE);
//            holder.day.setTextColor(Color.WHITE);
//            holder.dayName.setTextColor(Color.WHITE);
        } else {
            holder.line.setVisibility(View.INVISIBLE);
//            holder.day.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.btn_color));
//            holder.dayName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.btn_color));


        }

        holder.day.setText(plumberCalendar.getDay());
        holder.dayName.setText(plumberCalendar.getDayName().replace(".", ""));

    }


    @Override
    public int getItemCount() {
        return calendar.size();
    }


}
