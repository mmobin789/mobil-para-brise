package com.mobin.mobil.parabrise.interfaces;

import com.mobin.mobil.parabrise.model.PlumberJobs;

import java.util.List;

/**
 * Created by macbook on 23/11/2017.
 */

public interface OnListingListener {
    void onListingReceived(List<PlumberJobs> plumberJobs);

    void onListingFailed(String error);
}
