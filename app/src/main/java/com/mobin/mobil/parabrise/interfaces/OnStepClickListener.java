package com.mobin.mobil.parabrise.interfaces;

import com.mobin.mobil.parabrise.adapters.BaseRecyclerAdaper;
import com.mobin.mobil.parabrise.adapters.GeneralViewHolder;

/**
 * Created by macbook on 24/11/2017.
 */

public interface OnStepClickListener extends BaseRecyclerAdaper.OnListClickListener {
    void OnStepClicked(int stepNo, GeneralViewHolder holder);
}
