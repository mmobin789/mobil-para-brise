package com.mobin.mobil.parabrise.controllers

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.adapters.TabsAdapter
import com.mobin.mobil.parabrise.helpers.ExceptionDealer
import com.mobin.mobil.parabrise.location.LocationFetcher
import com.mobin.mobil.parabrise.model.PlumberCalendar
import com.mobin.mobil.parabrise.subcontrollers.BaseFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_bar.*
import java.util.*

class MainActivity : BaseActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0?.id) {
            home.id -> {
                pager.currentItem = 0
            }
            faq.id -> {
                pager.currentItem = 2
            }
            profile.id -> {
                pager.currentItem = 3
            }
            map.id -> {
                pager.currentItem = 1
            }
        }
    }

    override fun initViews() {

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 3 && permissions.isNotEmpty() && grantResults.isNotEmpty()) {
            BaseFragment.map?.enableLocation()
            fetchLocation()

        }
    }

    private fun fetchLocation() {
        startService(Intent(this, LocationFetcher().javaClass))
    }

    var list: MutableList<PlumberCalendar> = ArrayList()
    override fun setEvents() {
        pager.adapter = TabsAdapter(supportFragmentManager)
        pager.offscreenPageLimit = 4

        home.setOnClickListener(this)
        faq.setOnClickListener(this)
        profile.setOnClickListener(this)
        map.setOnClickListener(this)
        if (hasLocationPermission(this)) {
            fetchLocation()
        }


        //call("03217004104")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.setDefaultUncaughtExceptionHandler(ExceptionDealer())
        setContentView(R.layout.activity_main)
        super.onCreate(savedInstanceState)
    }
}
