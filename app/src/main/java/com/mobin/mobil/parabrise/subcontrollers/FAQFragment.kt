package com.mobin.mobil.parabrise.subcontrollers


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.adapters.BaseRecyclerAdaper
import com.mobin.mobil.parabrise.adapters.FAQAdapter
import com.mobin.mobil.parabrise.adapters.GeneralViewHolder
import com.mobin.mobil.parabrise.api.APIMobil
import com.mobin.mobil.parabrise.interfaces.OnAnswersListener
import com.mobin.mobil.parabrise.interfaces.OnFAQsListingListener
import com.mobin.mobil.parabrise.model.Answer
import com.mobin.mobil.parabrise.model.Question
import com.mobin.mobil.parabrise.utils.SLeekUI
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_faq.*


/**
 * A simple [Fragment] subclass.
 * Use the [FAQFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FAQFragment : BaseFragment(), OnFAQsListingListener, BaseRecyclerAdaper.OnListClickListener {

    override fun onItemClick(generalViewHolder: GeneralViewHolder?, position: Int) {


        idStep = when (step) {
            2 -> 59
            3 -> 60
            4 -> 61
            5 -> 62
            else -> questions!![position].parentID
        }


        APIMobil.getAnswers(idStep, object : OnAnswersListener {
            override fun onListingFailed(error: String) {

                step = -1
            }

            override fun onAnswersReceived(answers: MutableList<Answer>) {

                step = -1

                if (answers.size > 0) {
                    questions!![expandedPosition].answer.answer = ""
                    questions!![position].answer = answers[0]
                    adapter?.notifyItemChanged(expandedPosition)
                    expandedPosition = position
                    adapter?.notifyItemChanged(position)

                } else {
                    Toast.makeText(context, "No Answer Found", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onItemLongClick(position: Int): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var step = -1
    private var idStep = -1
    private var expandedPosition = 0
    private var adapter: FAQAdapter? = null
    private var questions: MutableList<Question>? = null

    override fun update(data: Bundle) {
        getMainActivity().pager.currentItem = 2
        step = data.getInt("step")
        getFAQs()


    }

    override fun onListingReceived(questions: MutableList<Question>) {


        adapter = FAQAdapter(questions)
        rv.adapter = adapter
        this.questions = questions
        adapter?.setOnItemClickListener(this)
    }


    override fun onListingFailed(error: String) {
        Log.e("FAQApiError", error)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_faq, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        SLeekUI.initRecyclerView(rv, true)
        getFAQs()

    }

    private fun getFAQs() {
        APIMobil.getFAQs(this)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @return A new instance of fragment FAQFragments.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(): FAQFragment {
            val fragment = FAQFragment()
//            val args = Bundle()
//            args.putString(ARG_PARAM1, param1)
//            args.putString(ARG_PARAM2, param2)
//            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
