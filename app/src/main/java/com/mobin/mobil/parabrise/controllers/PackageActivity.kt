package com.mobin.mobil.parabrise.controllers

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.model.JobPackage
import com.mobin.mobil.parabrise.utils.SLeekUI
import kotlinx.android.synthetic.main.activity_package.*

class PackageActivity : BaseActivity() {

    override fun initViews() {

    }

    override fun setEvents() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_package)
        super.onCreate(savedInstanceState)
    }

    fun goFullScreen(view: View) {

        startActivityForResult(Intent(this, FullScreenActivity::class.java), 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            car.setImageBitmap(SLeekUI.getBitmapFromList(0))
        }
    }

    fun send(view: View) {
        val f = fuel.text.toString()
        val m = km.text.toString()
        val rep = slope.text.toString()
        val d = newDate.text.toString()
        val c = comment.text.toString()

        if (!f.isEmpty() && !m.isEmpty() && !rep.isEmpty() && !d.isEmpty() && !c.isEmpty()) {
            SLeekUI.updateUI(send, R.string.success, R.string.send)
            JobDetailActivity.jobPackage = JobPackage(f, m, rep, d, c, System.currentTimeMillis() / 1000, "no image bro")
            setResult(RESULT_OK)
            onBackPressed()

        } else {
            SLeekUI.updateUI(send, "Remplir manquant", R.string.send)
            setResult(RESULT_CANCELED)
        }


    }

    fun cancel(view: View) {
        onBackPressed()
    }
}
