package com.mobin.mobil.parabrise.adapters;

import android.view.View;
import android.view.ViewGroup;

import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.interfaces.OnHelpClickListener;
import com.mobin.mobil.parabrise.interfaces.OnStepClickListener;
import com.mobin.mobil.parabrise.model.Status;

import java.util.List;

/**
 * Created by macbook on 24/11/2017.
 */

public class StepsAdapter extends BaseRecyclerAdaper {
    private List<Status> list;
    private OnHelpClickListener helpListener;
    private OnStepClickListener stepListener;

    public StepsAdapter(List<Status> list) {
        this.list = list;
    }

    public void setOnStepClickListener(OnStepClickListener stepListener) {
        this.stepListener = stepListener;
    }

    public void setOnHelpClickListener(OnHelpClickListener listener) {
        helpListener = listener;
    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        GeneralViewHolder h = new GeneralViewHolder(View.inflate(parent.getContext(), R.layout.adapter_steps, null), listener, GeneralViewHolder.AdapterType.steps);
        h.setCustomViewClickListener(h.step, stepListener);
        h.setCustomViewClickListener(h.help, helpListener);
        return h;
    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        Status status = list.get(position);
        holder.step.setText(status.getName());
        holder.stepNo.setText(String.valueOf(++position));
        updateUI(holder);


    }

    private void updateUI(GeneralViewHolder h) {
        switch (h.getAdapterPosition()) {
            case 1:
                h.line.setVisibility(View.VISIBLE);
                h.line_bottom.setVisibility(View.VISIBLE);
                h.help.setVisibility(View.VISIBLE);
                break;
            case 2:
                h.line.setVisibility(View.VISIBLE);
                h.line_bottom.setVisibility(View.INVISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
