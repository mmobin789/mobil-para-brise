package com.mobin.mobil.parabrise.subcontrollers


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.adapters.BaseRecyclerAdaper
import com.mobin.mobil.parabrise.adapters.DatesAdapter
import com.mobin.mobil.parabrise.adapters.GeneralViewHolder
import com.mobin.mobil.parabrise.adapters.JobsAdapter
import com.mobin.mobil.parabrise.api.APIMobil
import com.mobin.mobil.parabrise.controllers.JobDetailActivity
import com.mobin.mobil.parabrise.interfaces.OnListingListener
import com.mobin.mobil.parabrise.model.PlumberCalendar
import com.mobin.mobil.parabrise.model.PlumberJobs
import com.mobin.mobil.parabrise.utils.SLeekUI
import kotlinx.android.synthetic.main.fragment_home.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : BaseFragment(), OnListingListener {
    override fun update(data: Bundle) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var onJobsListener: OnListingListener? = null

    fun setOnJobsListener(onJobsListener: OnListingListener) {
        this.onJobsListener = onJobsListener
    }


    override fun onListingReceived(plumberJobs: MutableList<PlumberJobs>?) {
        loading.visibility = View.INVISIBLE
        if (plumberJobs != null && plumberJobs.size > 0) {
            rv2.visibility = View.VISIBLE
            updateUI(plumberJobs)

        } else {
            error.visibility = View.VISIBLE
            rv2.visibility = View.GONE
            error.setText(R.string.no_data)

        }
    }

    override fun onListingFailed(error: String?) {
        loading.visibility = View.INVISIBLE
        rv2.visibility = View.GONE
        this.error.visibility = View.VISIBLE
        this.error.setText(R.string.no_data)
        Log.e("ListingError", error)
    }


    var lastCheckedPosition = 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        SLeekUI.initRecyclerView(rv, false)
        SLeekUI.initRecyclerView(rv2, true)
        setDatesAdapter()
        hitListingAPI(lastCheckedPosition)
    }

    var list: MutableList<PlumberCalendar>? = null
    private fun setDatesAdapter() {
        list = getMainActivity().list
        list?.clear()
        val currentTimeC: Calendar = Calendar.getInstance()
        val dateFormat = SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE)
        val frenchDate = dateFormat.format(currentTimeC.time)
        header.text = SLeekUI.capWordFirstLetter(frenchDate)
        for (i in 0..6) {
            if (i == 0)
                currentTimeC.add(Calendar.DATE, -1)
            else
                currentTimeC.add(Calendar.DATE, 1)

            //   String dayOfTheWeek = dayNameFormat.format(currentTimeC.getTime());

            list?.add(PlumberCalendar(currentTimeC.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.FRANCE), currentTimeC.get(Calendar.DAY_OF_MONTH)))


        }
        list?.get(lastCheckedPosition)?.isSelected = true

        val adapter = DatesAdapter(list)
        rv.adapter = adapter

        adapter.setOnItemClickListener(object : BaseRecyclerAdaper.OnListClickListener {
            override fun onItemClick(generalViewHolder: GeneralViewHolder?, position: Int) {
                list?.get(lastCheckedPosition)?.isSelected = false
                list?.get(position)?.isSelected = true
                lastCheckedPosition = position
                adapter.notifyDataSetChanged()
                hitListingAPI(position)
            }

            override fun onItemLongClick(position: Int): Boolean {
                return false
            }
        })


    }

    private fun hitListingAPI(position: Int) {
        loading.visibility = View.VISIBLE
        rv2.visibility = View.GONE
        error.visibility = View.INVISIBLE
        val currentTimeC: Calendar = Calendar.getInstance()
        // currentTimeC.timeZone.id = "Europe/Paris"
        APIMobil.getListing(arrayOf(currentTimeC.get(Calendar.YEAR).toString() + "", (currentTimeC.get(Calendar.MONTH) + 1).toString() + "", list!![position].day), this)

    }

    private fun updateUI(plumberJobs: List<PlumberJobs>) {
        val adapter = JobsAdapter(plumberJobs)
        rv2.adapter = adapter


        adapter.setOnItemClickListener(object : BaseRecyclerAdaper.OnListClickListener {
            override fun onItemClick(generalViewHolder: GeneralViewHolder?, position: Int) {
                val p = plumberJobs[position]
                val detail = Intent(getMainActivity(), JobDetailActivity::class.java)
                detail.putExtra("plumberJob", p)
                startActivity(detail)
            }

            override fun onItemLongClick(position: Int): Boolean {
                return false
            }
        })

        onJobsListener?.onListingReceived(plumberJobs)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
//            val args = Bundle()
//            args.putString(ARG_PARAM1, param1)
//            args.putString(ARG_PARAM2, param2)
//            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
