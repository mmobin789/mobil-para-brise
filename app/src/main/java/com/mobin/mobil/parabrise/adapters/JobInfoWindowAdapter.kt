package com.mobin.mobil.parabrise.adapters

import android.content.Context
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.model.JobMarkerInfo


/**
 * Created by macbook on 26/12/2017.
 */
class JobInfoWindowAdapter(context: Context, jobMarkerInfo: JobMarkerInfo) : GoogleMap.InfoWindowAdapter {

    private val context: Context? = context
    private val jobInfo: JobMarkerInfo = jobMarkerInfo
    private var start: TextView? = null
    private var header: TextView? = null
    private var end: TextView? = null

    override fun getInfoContents(p0: Marker?): View? {
        val v: View = View.inflate(context, R.layout.marker_window, null)
        start = v.findViewById(R.id.start)
        end = v.findViewById(R.id.end)
        //  go = v.findViewById(R.id.go)
        header = v.findViewById(R.id.header)
        header?.text = (" " + jobInfo.header)
        start?.text = jobInfo.startTime
        end?.text = jobInfo.endTime
        return v
    }


    override fun getInfoWindow(p0: Marker?): View? {
        return null
    }
}