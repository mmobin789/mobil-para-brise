package com.mobin.mobil.parabrise.subcontrollers


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobin.mobil.parabrise.R
import kotlinx.android.synthetic.main.adapter_steps.*


/**
 * A simple [Fragment] subclass.
 * Use the [Step1Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Step1Fragment : BaseFragment() {
    override fun update(data: Bundle) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    // TODO: Rename and change types of parameters


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.adapter_steps, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        name.setText(R.string.step1)
        help.visibility = View.INVISIBLE
        no.text = "1"
//        name.setOnTouchListener { _, _ ->
//            view.isEnabled = false
//            val s = getString(R.string.step1) + "..."
//            name.text = s
//            getJobDetailActivity().hitStepsApi(1, null)
//            true
//        }
        root.getChildAt(1).setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                val s = getString(R.string.step1) + "..."
                name.text = s
                p0!!.isEnabled = false
                getJobDetailActivity().hitStepsApi(1)


            }


        })


    }
//    private fun getJobDetailActivity(): JobDetailActivity {
//        return activity as JobDetailActivity
//    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Step1Fragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(): Step1Fragment {
            val fragment = Step1Fragment()
            // val args = Bundle()
            //  args.putString(ARG_PARAM1, step)
            //    args.putInt(ARG_PARAM2, position);
            //  fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
