package com.mobin.mobil.parabrise.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by macbook on 23/11/2017.
 */

public class Extra implements Parcelable {

    public static final Parcelable.Creator<Extra> CREATOR = new Parcelable.Creator<Extra>() {
        @Override
        public Extra createFromParcel(Parcel source) {
            return new Extra(source);
        }

        @Override
        public Extra[] newArray(int size) {
            return new Extra[size];
        }
    };
    private String notification_datetime = "";
    private String notification_time = "";
    private String schedule_title = "";
    private String allow_notification = "";
    private String notification_time_interval = "";
    private String[] invite_contact = {""};
    private String notification_via = "";
    private String all_day = "";
    private String lat = "0.0", lng = "0.0";

    private Extra(Parcel in) {
        this.notification_datetime = in.readString();
        this.notification_time = in.readString();
        this.schedule_title = in.readString();
        this.allow_notification = in.readString();
        this.notification_time_interval = in.readString();
        this.invite_contact = in.createStringArray();
        this.notification_via = in.readString();
        this.all_day = in.readString();
        this.lat = in.readString();
        this.lng = in.readString();
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getNotification_datetime() {
        return notification_datetime;
    }

    public void setNotification_datetime(String notification_datetime) {
        this.notification_datetime = notification_datetime;
    }

    public String getNotification_time() {
        return notification_time;
    }

    public void setNotification_time(String notification_time) {
        this.notification_time = notification_time;
    }

    public String getSchedule_title() {
        return schedule_title;
    }

    public void setSchedule_title(String schedule_title) {
        this.schedule_title = schedule_title;
    }

    public String getAllow_notification() {
        return allow_notification;
    }

    public void setAllow_notification(String allow_notification) {
        this.allow_notification = allow_notification;
    }

    public String getNotification_time_interval() {
        return notification_time_interval;
    }

    public void setNotification_time_interval(String notification_time_interval) {
        this.notification_time_interval = notification_time_interval;
    }

    public String[] getInvite_contact() {
        return invite_contact;
    }

    public void setInvite_contact(String[] invite_contact) {
        this.invite_contact = invite_contact;
    }

    public String getNotification_via() {
        return notification_via;
    }

    public void setNotification_via(String notification_via) {
        this.notification_via = notification_via;
    }

    public String getAll_day() {
        return all_day;
    }

    public void setAll_day(String all_day) {
        this.all_day = all_day;
    }

    @Override
    public String toString() {
        return "ClassPojo [notification_datetime = " + notification_datetime + ", notification_time = " + notification_time + ", schedule_title = " + schedule_title + ", allow_notification = " + allow_notification + ", notification_time_interval = " + notification_time_interval + ", invite_contact = " + invite_contact + ", notification_via = " + notification_via + ", all_day = " + all_day + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.notification_datetime);
        dest.writeString(this.notification_time);
        dest.writeString(this.schedule_title);
        dest.writeString(this.allow_notification);
        dest.writeString(this.notification_time_interval);
        dest.writeStringArray(this.invite_contact);
        dest.writeString(this.notification_via);
        dest.writeString(this.all_day);
        dest.writeString(this.lat);
        dest.writeString(this.lng);
    }
}
