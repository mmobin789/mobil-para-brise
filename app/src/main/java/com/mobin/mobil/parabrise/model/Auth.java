package com.mobin.mobil.parabrise.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macbook on 22/11/2017.
 */

public class Auth {
    @SerializedName("data")
    private User user;
    @SerializedName("username")
    private String name;
    private String password, message;

    public Auth(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getMessage() {
        return message;
    }
}
