//package com.mobin.mobil.parabrise.subcontrollers;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.mobin.mobil.parabrise.R;
//
///**
// * Created by macbook on 19/12/2017.
// */
//
//public class Step3FragmentJava extends BaseFragment {
//
//
//    public Step3FragmentJava() {
//        // Required empty public constructor
//    }
//
//
//    // TODO: Rename and change types and number of parameters
//    @NonNull
//    public static Step3Fragment newInstance() {
//
//        return new Step3Fragment();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        final TextView name = view.findViewById(R.id.name);
//        TextView no = view.findViewById(R.id.no);
//        no.setText("3");
//        Button help = view.findViewById(R.id.help);
//        name.setText(R.string.step3);
//        name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String s = getString(R.string.step3) + "...";
//                name.setText(s);
//                getJobDetailActivity().hitStepsApi(3, null);
//            }
//        });
//    }
//
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.adapter_steps, container, false);
//
//
//    }
//
//}
