package com.mobin.mobil.parabrise.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.api.APIMobil;
import com.mobin.mobil.parabrise.helpers.AppPersistence;
import com.mobin.mobil.parabrise.interfaces.OnLoginListener;
import com.mobin.mobil.parabrise.model.Auth;
import com.mobin.mobil.parabrise.model.User;

import static com.mobin.mobil.parabrise.utils.SLeekUI.updateUI;

public class LoginActivity extends BaseActivity implements OnLoginListener {
    EditText etName, etPass;
    Button login;

    @Override
    protected void initViews() {
        etName = findViewById(R.id.name);
        etPass = findViewById(R.id.password);
    }

    @Override
    protected void setEvents() {

    }

    public void forgotPassword(View v) {
        startActivity(new Intent(v.getContext(), ForgotPasswordActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
    }

    public void login(View v) {
        login = (Button) v;
        String name = etName.getText().toString().trim();
        String password = etPass.getText().toString().trim();

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(password)) {
            updateUI(login, R.string.input, R.string.login);
        } else {
            updateUI(login, R.string.logging_in, R.string.login);
            APIMobil.login(this, new Auth(name, password), this);
        }
    }

    @Override
    public void OnLoginSuccess(User user) {
        updateUI(login, R.string.success, R.string.login);
        AppPersistence.saveUser(this, user);
        onBackPressed();
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
    }

    @Override
    public void OnLoginFail(String error) {
        updateUI(login, error, R.string.login);
    }


}
