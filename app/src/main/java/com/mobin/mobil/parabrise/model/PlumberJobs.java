package com.mobin.mobil.parabrise.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by macbook on 23/11/2017.
 */

public class PlumberJobs implements Parcelable {
    public static final Parcelable.Creator<PlumberJobs> CREATOR = new Parcelable.Creator<PlumberJobs>() {
        @Override
        public PlumberJobs createFromParcel(Parcel source) {
            return new PlumberJobs(source);
        }

        @Override
        public PlumberJobs[] newArray(int size) {
            return new PlumberJobs[size];
        }
    };
    private String created_by = "";
    private Extra extra;
    private String type = "";
    private Contact contact;
    private String id = "";
    private String message = "";
    private String end_date = "";
    private String updated_at = "";
    private String sent_notification = "";
    private String created_at = "";
    private String user_id = "";
    private String email_subject = "";
    private String start_date = "";
    private String log_type = "";

    private PlumberJobs(Parcel in) {
        this.created_by = in.readString();
        this.extra = in.readParcelable(Extra.class.getClassLoader());
        this.type = in.readString();
        this.contact = in.readParcelable(Contact.class.getClassLoader());
        this.id = in.readString();
        this.message = in.readString();
        this.end_date = in.readString();
        this.updated_at = in.readString();
        this.sent_notification = in.readString();
        this.created_at = in.readString();
        this.user_id = in.readString();
        this.email_subject = in.readString();
        this.start_date = in.readString();
        this.log_type = in.readString();
    }

    public PlumberJobs() {
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Extra getExtra() {
        return extra;
    }

    public void setExtra(Extra extra) {
        this.extra = extra;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getSent_notification() {
        return sent_notification;
    }

    public void setSent_notification(String sent_notification) {
        this.sent_notification = sent_notification;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail_subject() {
        return email_subject;
    }

    public void setEmail_subject(String email_subject) {
        this.email_subject = email_subject;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getLog_type() {
        return log_type;
    }

    public void setLog_type(String log_type) {
        this.log_type = log_type;
    }

    @Override
    public String toString() {
        return "ClassPojo [created_by = " + created_by + ", extra = " + extra + ", type = " + type + ", contact = " + contact + ", id = " + id + ", message = " + message + ", end_date = " + end_date + ", updated_at = " + updated_at + ", sent_notification = " + sent_notification + ", created_at = " + created_at + ", user_id = " + user_id + ", email_subject = " + email_subject + ", start_date = " + start_date + ", log_type = " + log_type + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.created_by);
        dest.writeParcelable(this.extra, flags);
        dest.writeString(this.type);
        dest.writeParcelable(this.contact, flags);
        dest.writeString(this.id);
        dest.writeString(this.message);
        dest.writeString(this.end_date);
        dest.writeString(this.updated_at);
        dest.writeString(this.sent_notification);
        dest.writeString(this.created_at);
        dest.writeString(this.user_id);
        dest.writeString(this.email_subject);
        dest.writeString(this.start_date);
        dest.writeString(this.log_type);
    }
}
