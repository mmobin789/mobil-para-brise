package com.mobin.mobil.parabrise.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mobin.mobil.parabrise.controllers.BaseActivity;
import com.mobin.mobil.parabrise.model.User;

/**
 * Created by macbook on 22/11/2017.
 */

public class AppPersistence {
    private static final String key_token = "token";
    private static final String key_email = "email";
    // private static final String key_id = "id";
    private static final String key_name = "name";
    private static final String key_role = "role";
    public static String token = "";
    public static String email = "";
    public static String name = "";
    public static String role = "";
    public static boolean userLogged;

    private AppPersistence() {
    }


    public static void clearUser(Context context) {
        getPreferences(context).edit().clear().apply();
    }

    public static void saveUser(Context context, User user) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(key_name, user.name);
        editor.putString(key_token, user.token);
        editor.putString(key_email, user.email);
//        editor.putString(key_role, user.roles.get(0));
        editor.apply();
        checkUser(context);
    }

    public static void checkUser(Context context) {
        SharedPreferences preferences = getPreferences(context);
        token = "Bearer " + preferences.getString(key_token, "");
        name = preferences.getString(key_name, "");
        email = preferences.getString(key_email, "");
        role = preferences.getString(key_role, "");
        Log.i("token", token);
        //  userName = getPreferences(context).getString(key_name, "");

        userLogged = token.length() > 7;

    }

    public static void saveInt(Context context, String jobID, int step) {
        //  Toast.makeText(context, step + " saved", Toast.LENGTH_SHORT).show();
        getPreferences(context).edit().putInt(jobID, step).apply();
    }

    public static int getInt(BaseActivity context, String jobID) {
        //   Toast.makeText(context, val + " fetched", Toast.LENGTH_SHORT).show();
        return getPreferences(context).getInt(jobID, -1);

    }

    private static SharedPreferences getPreferences(Context context) {

        return PreferenceManager.getDefaultSharedPreferences(context);

    }
}
