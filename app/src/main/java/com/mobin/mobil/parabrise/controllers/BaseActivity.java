package com.mobin.mobil.parabrise.controllers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.mobin.mobil.parabrise.R;

/**
 * Created by macbook on 22/11/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    public static synchronized GoogleApiClient buildGoogleApiClient(Context context, GoogleApiClient.ConnectionCallbacks callbacks, GoogleApiClient.OnConnectionFailedListener connectionFailedListener) {
        //   Toast.makeText(this,"buildGoogleApiClient",Toast.LENGTH_SHORT).show();
        //   pGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Places.GEO_DATA_API).addApi(Places.PLACE_DETECTION_API).enableAutoManage(this,this).build();


        return new GoogleApiClient.Builder(context).addConnectionCallbacks(callbacks).addOnConnectionFailedListener(connectionFailedListener).addApi(LocationServices.API).build();


    }

    public static LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(50000); //5 seconds
        locationRequest.setFastestInterval(5000); //3 seconds
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setSmallestDisplacement(0.1F); //1/10 meter
        return locationRequest;
    }

    public static void zoomToLocation(GoogleMap googleMap, double lat, double lng) {

        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        googleMap.animateCamera(cameraUpdate);

    }

    public static boolean hasLocationPermission(BaseActivity baseActivity) {
        boolean permission = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            if (!permission) {

                baseActivity.requestPermissions(new String[]{Manifest
                        .permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 3);
            }
        }
        return permission;
    }

    public void call(String number) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number)));
    }

    abstract void initViews();

    abstract void setEvents();

    public void setMapUI(OnMapReadyCallback callback, int containerID) {
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(containerID);
        supportMapFragment.getMapAsync(callback);
    }

    public void setMapUI(Fragment fragment, OnMapReadyCallback onMapReadyCallback) {
        SupportMapFragment supportMapFragment = (SupportMapFragment) fragment.getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(onMapReadyCallback);
    }

    public void setMapUI(Fragment fragment, OnMapReadyCallback onMapReadyCallback, int fragmentContainerID) {
        SupportMapFragment supportMapFragment = (SupportMapFragment) fragment.getChildFragmentManager().findFragmentById(fragmentContainerID);
        supportMapFragment.getMapAsync(onMapReadyCallback);
    }

    public TextView getLabelView(int id) {
        return findViewById(id);
    }

    public EditText getLabelViewInput(int id) {
        return findViewById(id);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        setEvents();
    }

    public void restartActivity(BaseActivity activity) {
        activity.finish();
        startActivity(new Intent(this, activity.getClass()));

    }

    public void restartActivity(BaseActivity activity, Parcelable parcelable, String key) {
        activity.finish();
        Intent given = new Intent(this, activity.getClass());
        given.putExtra(key, parcelable);
        startActivity(given);

    }

    public void restartApp(final BaseActivity current) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                current.finish();
                Intent i = new Intent(BaseActivity.this, SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            }
        }, 2000);

    }

    public void killApp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            super.finishAndRemoveTask();
        } else {
            super.finish();
        }
    }
}
