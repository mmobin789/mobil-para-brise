package com.mobin.mobil.parabrise.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by macbook on 22/11/2017.
 */

public class PlumberCalendar {
    @SerializedName("data")
    public List<PlumberJobs> plumberJobs;
    private String dayName;
    private int day;
    private boolean selected;

    public PlumberCalendar(String dayName, int day) {
        this.dayName = dayName;
        this.day = day;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDayName() {
        return dayName;
    }

    public String getDay() {
        return day + "";
    }
}
