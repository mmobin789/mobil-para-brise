package com.mobin.mobil.parabrise.location

import android.Manifest
import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.mobin.mobil.parabrise.api.APIMobil
import com.mobin.mobil.parabrise.controllers.BaseActivity
import com.mobin.mobil.parabrise.model.User


class LocationFetcher : Service(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //private var googleClient: GoogleApiClient? = null


    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnected(p0: Bundle?) {
        startLocationUpdate()
    }

    private fun startLocationUpdate() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(BaseActivity.getLocationRequest(), locationCallback, null)
        }
    }

    private fun stopLocationUpdate() {
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)

    }


    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        BaseActivity.buildGoogleApiClient(this, this, this).connect()

        Log.i("onStartCommand", "runs")
        // Log.i("googleClient", googleClient!!.isConnected.toString())
        return START_STICKY
    }

    private fun sendLocation(location: LocationResult?) {
        val delay: Long = 300000
        val lat = location?.lastLocation?.latitude
        val lng = location?.lastLocation?.longitude
        User.lat = lat!!
        User.lng = lng!!
        Log.i("Location", lat.toString() + "/" + lng)
        APIMobil.sendLocation(location.lastLocation!!.latitude, location.lastLocation.longitude)
        Handler().postDelayed({
            Log.i("locationDelay", "After 5 min")
            startLocationUpdate()
        }, delay)
    }

    //   private var location: LocationResult? = null
    private val locationCallback = object : LocationCallback() {


        override fun onLocationResult(locationResult: LocationResult?) {

            //  location = locationResult
            sendLocation(locationResult)
        }
    }

    override fun onDestroy() {
        stopLocationUpdate()
    }
}
