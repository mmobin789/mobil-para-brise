package com.mobin.mobil.parabrise.interfaces;

import com.mobin.mobil.parabrise.model.User;

/**
 * Created by macbook on 22/11/2017.
 */

public interface OnLoginListener {
    void OnLoginSuccess(User user);

    void OnLoginFail(String error);


}
