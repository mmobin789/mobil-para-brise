package com.mobin.mobil.parabrise.controllers;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.adapters.GeneralViewHolder;
import com.mobin.mobil.parabrise.adapters.StepsAdapter;
import com.mobin.mobil.parabrise.adapters.StepsPagerAdapter;
import com.mobin.mobil.parabrise.api.APIMobil;
import com.mobin.mobil.parabrise.helpers.AppPersistence;
import com.mobin.mobil.parabrise.helpers.VerticalViewPager;
import com.mobin.mobil.parabrise.interfaces.OnStepClickListener;
import com.mobin.mobil.parabrise.interfaces.OnStepCompleteListener;
import com.mobin.mobil.parabrise.model.Contact;
import com.mobin.mobil.parabrise.model.JobPackage;
import com.mobin.mobil.parabrise.model.PlumberJobs;
import com.mobin.mobil.parabrise.model.Status;
import com.mobin.mobil.parabrise.utils.SLeekUI;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class JobDetailActivity extends BaseActivity implements OnStepCompleteListener {

    public static PlumberJobs plumberJobs;
    public static JobPackage jobPackage;
    Button step1, step2, step3;
    RecyclerView stepsRecycler;
    VerticalViewPager verticalStepsPager;
    //   RecyclerView.SmoothScroller smoothScroller;
    //LinearLayoutManager layoutManager;
    // boolean isStep2Validated = false;
    ScrollView root;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_job_detail);
        super.onCreate(savedInstanceState);
    }

    public void backPressed(View v) {
        onBackPressed();
    }

    @Override
    protected void initViews() {
//        root = findViewById(R.id.root);
//        scrollView = findViewById(R.id.sv);
//        step1 = findViewById(R.id.step1);
//        step2 = findViewById(R.id.step2);
//        step3 = findViewById(R.id.step3);
//        help = findViewById(R.id.help);
        //stepsRecycler = findViewById(R.id.stepsR);
        verticalStepsPager = findViewById(R.id.pager);
        verticalStepsPager.setAdapter(new StepsPagerAdapter(getSupportFragmentManager()));
        //  setStepsRecycler();

        plumberJobs = getIntent().getParcelableExtra("plumberJob");
        Contact contact = plumberJobs.getContact();
        getLabelView(R.id.title).setText(plumberJobs.getExtra().getSchedule_title());
        getLabelView(R.id.header).setText(plumberJobs.getExtra().getSchedule_title());
        getLabelView(R.id.name).setText(String.format("%s %s", contact.getFirst_name(), contact.getLast_name()));
        getLabelView(R.id.loc).setText(String.format("%s %s %s %s", contact.getStreet_1(), contact.getCity(), contact.getState(), contact.getCountry()));
        getLabelView(R.id.notes).setText(SLeekUI.stripHtml(contact.getNotes()));
        Log.i("notes", contact.getNotes());
        checkJobTimeValidity();


    }


    private void setLastStep() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                moveToStep(AppPersistence.getInt(JobDetailActivity.this, plumberJobs.getId()));
            }
        }, 1000);
    }

    private void setStepsRecycler() {
//        smoothScroller = new LinearSmoothScroller(this) {
//            @Override
//            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
//                return 300f / displayMetrics.densityDpi;
//            }
//
//            @Override
//            protected int getVerticalSnapPreference() {
//                return LinearSmoothScroller.SNAP_TO_START;
//            }
//        };
        SLeekUI.initRecyclerViewNoNScrollable(stepsRecycler, true);
        List<Status> statusList = new ArrayList<>();
        StepsAdapter adapter;
        String[] names = {getString(R.string.step1), getString(R.string.step2), getString(R.string.step3)};
        for (String s : names)
            statusList.add(new Status(s));
        adapter = new StepsAdapter(statusList);
        stepsRecycler.setAdapter(adapter);
        adapter.setOnStepClickListener(new OnStepClickListener() {
            @Override
            public void OnStepClicked(int stepNo, GeneralViewHolder holder) {
                switch (stepNo) {
                    case 1:
                        step1.performClick();
                        break;
                    case 2:
                        step2.performClick();
                        break;
                    case 3:
                        step3.performClick();
                        break;
                }
            }

            @Override
            public void onItemClick(GeneralViewHolder generalViewHolder, int position) {

            }

            @Override
            public boolean onItemLongClick(int position) {
                return false;
            }
        });

    }

    private void checkJobTimeValidity() {
        Date jobStartDate = SLeekUI.getFrenchTimeFromString(plumberJobs.getStart_date());
        Date jobEndDate = SLeekUI.getFrenchTimeFromString(plumberJobs.getEnd_date());
        Calendar currentDate = Calendar.getInstance();
        Calendar jobStart = Calendar.getInstance();
        jobStart.setTime(jobStartDate);
        jobStart.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));
        Calendar jobEnd = Calendar.getInstance();
        jobEnd.setTime(jobEndDate);
        jobEnd.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));

        Log.i("JobStartDateServer", plumberJobs.getStart_date());
        Log.i("JobEndDateServer", plumberJobs.getEnd_date());
        Log.i("JobStartDate", jobStart.getTime().toString());
        Log.i("JobEndDate", jobEnd.getTime().toString());
        Log.i("currentDate", currentDate.getTime().toString());
        //   SimpleDateFormat minutesFormatter = new SimpleDateFormat("mm", Locale.FRANCE);
        // String jobMinutes = minutesFormatter.format(jobStartDate);
        long remainderTime;
        if (currentDate.getTime().after(jobStart.getTime())) {
            remainderTime = 0;
        } else {
            //  remainderTime = Integer.parseInt(minutesFormatter.format(new Date(jobEndDate.getTime() - jobStartDate.getTime())));
            long diff = jobEnd.getTime().getTime() - currentDate.getTime().getTime();
            remainderTime = (diff / 1000) / 60;
            if (remainderTime < 0)
                remainderTime = 0;
        }
        String time = getString(R.string.time) + ": " + remainderTime + " Min";
        getLabelView(R.id.time).setText(time);

        if (System.currentTimeMillis() == jobStart.getTimeInMillis() + (24 * 60 * 60 * 1000)) {
            findViewById(R.id.steps).setEnabled(false);
            Log.i("stepsDisabled", "disabled");
        }


    }

    public void getDirections(View v) {
        Contact contact = plumberJobs.getContact();
        String address = contact.getStreet_1() + " " + contact.getCity() + " " + contact.getState() + " " + contact.getCountry();
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=" + address));
        startActivity(intent);
    }

    public void sendSMS(View v) {
        String number = plumberJobs.getContact().getPhone();  // The number on which you want to send SMS
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
    }

    public void hitStepsApi(int stepNo) {
        APIMobil.stepsApi(plumberJobs.getId(), stepNo, this);
    }


    public void call(View v) {
        Intent intent = getPackageManager().getLaunchIntentForPackage("com.tcx.sipphone14");
        if (intent != null) {
            // We found the activity now start the activity
            intent.setData(Uri.parse("tel:" + plumberJobs.getContact().getPhone()));

        } else {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);

            intent.setData(Uri.parse("market://details?id=com.tcx.sipphone14"));
            startActivity(intent);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void setEvents() {

        setLastStep();
//
//        step1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//                //step2.getParent().requestChildFocus(step2, step2);
//
//                step1.setText("" + getString(R.string.step1) + "...");
//                step1.setEnabled(false);
//                hitStepsApi(1);
//
//            }
//        });
//
//        step2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                step2.setText("" + getString(R.string.step2) + "...");
//                step2.setEnabled(false);
//                hitStepsApi(2);
//
//            }
//        });
//
//        step3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                step3.setText("" + getString(R.string.step3) + "...");
//                step3.setEnabled(false);
//                hitStepsApi(3);
//
//            }
//        });
//        help.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

//        List<Status> statusList = new ArrayList<>();
//        String[] data = {getString(R.string.step1), getString(R.string.step2), getString(R.string.step3)};
//        for (String aData : data) {
//            statusList.add(new Status(aData));
//        }
//        StepsAdapter adapter = new StepsAdapter(statusList);
//        adapter.setOnHelpClickListener(this);
//        adapter.setOnStepClickListener(this);
        //     recyclerView.setAdapter(adapter);


    }

    private void showItemDialog(final int stepNo) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //      dialog.setContentView(R.layout.activity_package);
        //  TextInputLayout fuel = dialog.findViewById(R.id.fuel);
        //  TextInputLayout km = dialog.findViewById(R.id.km);
        // final TextInputEditText etFuel = (TextInputEditText) fuel.getEditText();
        // final TextInputEditText etKm = (TextInputEditText) km.getEditText();
        final EditText etFuel = dialog.findViewById(R.id.fuel);
        final EditText etKm = dialog.findViewById(R.id.km);
        dialog.findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean b;
                String fuel = etFuel.getText().toString();
                String km = etKm.getText().toString();
                b = fuel.length() > 0 && km.length() > 0;

                if (!b)
                    Toast.makeText(view.getContext(), R.string.no_data, Toast.LENGTH_SHORT).show();
                else {
                    // if (stepNo == 2) {
                    // isStep2Validated = true;
                    //     isStep3Validated = false;
                    // else {
                    // isStep2Validated = false;
                    //   isStep3Validated = true;
                    // }
                    //  jobPackage = new TravelPackage(fuel, km, System.currentTimeMillis() / 1000);
                    //hitStepsApi(stepNo);
                    dialog.dismiss();

                }


            }
        });

        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    //    private void scrollToView(final ViewGroup scrollableContent, final View viewToScroll) {
//        long delay = 100; //delay to let finish with possible modifications to ScrollView
//        scrollView.postDelayed(new Runnable() {
//            public void run() {
//                Rect viewToScrollRect = new Rect(); //coordinates to scroll to
//                viewToScroll.getHitRect(viewToScrollRect); //fills viewToScrollRect with coordinates of viewToScroll relative to its parent (LinearLayout)
//                scrollView.requestChildRectangleOnScreen(scrollableContent, viewToScrollRect, false); //ScrollView will make sure, the given viewToScrollRect is visible
//            }
//        }, delay);
//    }
//
//    private void scroll2Child(final View view) {
//        scrollView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Rect rect = new Rect(0, 0, view.getWidth(), view.getHeight());
//                view.requestRectangleOnScreen(rect, false);
//            }
//        }, 100);
//
//    }
    private void moveToStep(int stepNo) {
        switch (stepNo) {
            case 2:
                verticalStepsPager.setCurrentItem(1, true);
                break;
            case 3:
                verticalStepsPager.setCurrentItem(2, true);
                break;
            case 4:
                verticalStepsPager.setCurrentItem(3, true);
                break;
            case 5:
                verticalStepsPager.setCurrentItem(4, true);
                break;

        }
    }
//    private void moveToStep(int stepNo, boolean firstTime) {
//        switch (stepNo) {
//            case 2:
//                scrollView.smoothScrollBy(0, 168);
//                //    scroll2Child(findViewById(R.id.circle2));
//                //  scrollView.smoothScrollBy(0, line1.getTop() * 2);
//                //  scrollToView(child2, line2);
//                break;
//            case 3:
//                if (firstTime)
//                    scrollView.smoothScrollBy(0, 355);
//                if (!firstTime && !isStep3Validated)
//                    restartActivity(this, plumberJobs, "plumberJob");
//                break;
//
//        }
//    }

//    private void moveToStep(int stepNo) {
//        switch (stepNo) {
//            case 2:
//                stepsRecycler.smoothScrollToPosition(1);
//                break;
//            case 3:
//                stepsRecycler.smoothScrollToPosition(2);
//                break;
//        }
//    }


    @Override
    public void OnStepCompleted(int stepNo) {

        if (stepNo == 1) {
            moveToStep(2);
            AppPersistence.saveInt(this, plumberJobs.getId(), 2);
        } else if (stepNo == 2) {
            moveToStep(3);
            AppPersistence.saveInt(this, plumberJobs.getId(), 3);
        } else if (stepNo == 3) {
            moveToStep(4);
            AppPersistence.saveInt(this, plumberJobs.getId(), stepNo);

        } else if (stepNo == 4) {
            moveToStep(5);
            AppPersistence.saveInt(this, plumberJobs.getId(), stepNo);
        } else {
            Toast.makeText(this, R.string.success, Toast.LENGTH_SHORT).show();
            AppPersistence.saveInt(this, plumberJobs.getId(), stepNo);
            onBackPressed();
        }


    }

    public void openPackageUI(int stepNo) {
        Intent packageDetail = new Intent(this, PackageActivity.class);
        //packageDetail.putExtra("step", stepNo);
        startActivityForResult(packageDetail, stepNo);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == 4 && resultCode == RESULT_OK) {
            hitStepsApi(4);
        } else if (requestCode == 3 && resultCode == RESULT_OK) {
            hitStepsApi(3);
        }
    }

    @Override
    public void OnStepFailed(int step, String error) {

    }


}
