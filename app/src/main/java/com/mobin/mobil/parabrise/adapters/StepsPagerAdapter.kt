package com.mobin.mobil.parabrise.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.mobin.mobil.parabrise.subcontrollers.*

/**
 * Created by macbook on 27/12/2017.
 */
class StepsPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {


    override fun getCount(): Int {
        return 5
    }

    override fun getItem(position: Int): Fragment {
        val fragment: BaseFragment
        when (position) {
            1 -> fragment = Step2Fragment.newInstance()
            2 -> fragment = Step3Fragment.newInstance()
            3 -> fragment = Step4Fragment.newInstance()
            4 -> fragment = Step5Fragment.newInstance()

            else -> fragment = Step1Fragment.newInstance()
        }
        return fragment
    }

}