package com.mobin.mobil.parabrise.adapters

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobin.mobil.parabrise.R
import com.mobin.mobil.parabrise.model.Question

/**
 * Created by macbook on 27/12/2017.
 */
class FAQAdapter(val questions: MutableList<Question>) : BaseRecyclerAdaper() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GeneralViewHolder {
        return GeneralViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.adapter_questions, parent, false), listener, GeneralViewHolder.AdapterType.faqs)
    }

    override fun getItemCount(): Int {
        return questions.size
    }

    override fun onBindViewHolder(holder: GeneralViewHolder?, position: Int) {
        val question = questions[position]
        val answer = question.answer.answer
        holder?.jobName?.text = question.question

        if (TextUtils.isEmpty(answer)) {
            holder?.jobDesc?.visibility = View.GONE
        } else {
            holder?.jobDesc?.visibility = View.VISIBLE
            holder?.jobDesc?.text = answer
        }
    }


}