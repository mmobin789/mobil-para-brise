package com.mobin.mobil.parabrise.model

import com.google.gson.annotations.SerializedName

/**
 * Created by macbook on 27/12/2017.
 */
class Answer {

    val id = -1
    @SerializedName("slug")
    var answer = ""
}