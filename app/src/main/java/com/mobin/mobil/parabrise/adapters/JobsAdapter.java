package com.mobin.mobil.parabrise.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.model.Contact;
import com.mobin.mobil.parabrise.model.PlumberJobs;
import com.mobin.mobil.parabrise.utils.SLeekUI;

import java.util.List;

/**
 * Created by macbook on 23/11/2017.
 */

public class JobsAdapter extends BaseRecyclerAdaper {

    private List<PlumberJobs> list;

    public JobsAdapter(List<PlumberJobs> list) {
        this.list = list;
    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        GeneralViewHolder holder = new GeneralViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_jobs_new, parent, false), listener, GeneralViewHolder.AdapterType.jobs);
        holder.setList(list);
        return holder;
    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {
        PlumberJobs plumberJobs = list.get(position);
        Contact contact = plumberJobs.getContact();
        holder.jobCreatedAt.setText(SLeekUI.formattedFrenchDate(plumberJobs.getStart_date()));
        holder.jobEndedAt.setText(SLeekUI.formattedFrenchDate(plumberJobs.getEnd_date()));
        holder.jobName.setText(plumberJobs.getExtra().getSchedule_title());
        holder.jobDesc.setText(SLeekUI.stripHtml(plumberJobs.getMessage()));
        holder.customerName.setText(String.format("%s %s", contact.getFirst_name(), contact.getLast_name()));
        holder.customerType.setText(plumberJobs.getType());
        // holder.address.setText(contact.getStreet_1() + " " + contact.getCity() + " " + contact.getState() + " " + contact.getCountry());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
