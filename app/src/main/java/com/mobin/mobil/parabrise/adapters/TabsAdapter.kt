package com.mobin.mobil.parabrise.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.mobin.mobil.parabrise.subcontrollers.*

/**
 * Created by macbook on 26/12/2017.
 */
class TabsAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val baseFragment: BaseFragment
        when (position) {

            1 -> {
                baseFragment = MapFragment.newInstance()
                BaseFragment.map = baseFragment
            }
            2 -> {
                baseFragment = FAQFragment.newInstance()
                BaseFragment.faq = baseFragment
            }
            3 -> {
                baseFragment = ProfileFragment.newInstance()
                BaseFragment.account = baseFragment
            }
            else -> {
                baseFragment = HomeFragment.newInstance()
                BaseFragment.home = baseFragment

            }
        }
        return baseFragment

    }

    override fun getCount(): Int {
        return 4
    }
}