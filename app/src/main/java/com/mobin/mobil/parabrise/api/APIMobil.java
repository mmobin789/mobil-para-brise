package com.mobin.mobil.parabrise.api;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.mobin.mobil.parabrise.R;
import com.mobin.mobil.parabrise.helpers.AppPersistence;
import com.mobin.mobil.parabrise.interfaces.OnAnswersListener;
import com.mobin.mobil.parabrise.interfaces.OnEmailRequestListener;
import com.mobin.mobil.parabrise.interfaces.OnFAQsListingListener;
import com.mobin.mobil.parabrise.interfaces.OnListingListener;
import com.mobin.mobil.parabrise.interfaces.OnLoginListener;
import com.mobin.mobil.parabrise.interfaces.OnStepCompleteListener;
import com.mobin.mobil.parabrise.model.Answer;
import com.mobin.mobil.parabrise.model.Auth;
import com.mobin.mobil.parabrise.model.PlumberCalendar;
import com.mobin.mobil.parabrise.model.Question;
import com.mobin.mobil.parabrise.model.User;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mobin.mobil.parabrise.controllers.JobDetailActivity.jobPackage;

/**
 * Created by macbook on 22/11/2017.
 */

public final class APIMobil {
    private static final String baseURL = "https://admin.mobilparebrise.fr/wp-json/";
    private static Retrofit retrofit;
    // private static String baseURL = "https://www.mobilparebrise.fr/rest/";


    private APIMobil() {
    }

    private static API getAPI() {

//
//        OkHttpClient client = new OkHttpClient.Builder()
//                .connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build();


        if (retrofit == null) {


//            Gson gson = new GsonBuilder()
//                    .setLenient().create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit.create(API.class);
    }


    //    public static void getJobListings(Context context, String[] data, final OnListingListener listener) {
//        String url = baseURL + "rdv/v1/getall/" + data[0] + "/" + data[1] + "/" + data[2];
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new
//                com.android.volley.Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        PlumberCalendar plumberCalendar = new Gson().fromJson(response + "", PlumberCalendar.class);
//                        listener.onListingReceived(plumberCalendar.plumberJobs);
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                listener.onListingFailed(error.toString());
//            }
//
//
//        }
//        ) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Authorization", AppPersistence.token);
//                return headers;
//            }
//        };
//
//        RequestQueue queue = Volley.newRequestQueue(context);
//        queue.add(jsonObjectRequest);
//
//    }
    public static void forgotPassword(final OnEmailRequestListener listener, String email) {
        String url = " technicien/v1/password/" + email;
        getAPI().forgotPassword(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.i("forgotPasswordAPI", response + "");
                if (response.isSuccessful())
                    try {
                        listener.onForgotPasswordApiResponse(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

            }
        });
    }

    public static void getListing(final String[] data, final OnListingListener listener) {

        String url = baseURL + "rdv/v1/getall/" + data[0] + "/" + data[1] + "/" + data[2];
        Log.i("getListingURL", url);
        getAPI().getListing(AppPersistence.token, url).enqueue(new Callback<PlumberCalendar>() {
            @Override
            public void onResponse(@NonNull Call<PlumberCalendar> call, @NonNull Response<PlumberCalendar> response) {
                Log.i("getListingsAPI", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    listener.onListingReceived(response.body().plumberJobs);
                } else {
                    Auth error = parseError(response);
                    listener.onListingFailed(error.getMessage());


                }
            }

            @Override
            public void onFailure(@NonNull Call<PlumberCalendar> call, @NonNull Throwable t) {
                listener.onListingFailed(t.toString());
            }
        });
    }


    public static void getAnswers(int parentID, final OnAnswersListener listener) {
        String url = baseURL + "wp/v2/dwkb?dwkb_category=" + parentID;
        //  final List<Answer> answerList = new ArrayList<>();
        Log.i("AnswersApiURL", url);
        getAPI().getAnswers(url).enqueue(new Callback<List<Answer>>() {
            @Override
            public void onResponse(@NonNull Call<List<Answer>> call, @NonNull Response<List<Answer>> response) {
                Log.i("answersApi", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    listener.onAnswersReceived(response.body());
                } else {
                    try {
                        listener.onListingFailed(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Answer>> call, @NonNull Throwable t) {
                listener.onListingFailed(t.toString());
                Log.i("answersApiError", t.toString());
            }
        });


    }

    public static void getFAQs(final OnFAQsListingListener listener) {
        String url = baseURL + "wp/v2/dwkb_category";
        Log.i("getFAQsUrl", url);
        getAPI().getFAQs(url).enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(@NonNull Call<List<Question>> call, @NonNull Response<List<Question>> response) {
                Log.i("FAQsAPI", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    listener.onListingReceived(response.body());
                } else {
                    try {
                        listener.onListingFailed(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Question>> call, @NonNull Throwable t) {
                listener.onListingFailed(t.toString());
            }
        });
    }

    public static void sendLocation(double lat, double lng) {
        String url = baseURL + "technicien/v1/setPosition/" + lat + "/" + lng;
        getAPI().sendLocation(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.i("sendLocationApi", response + "");
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e("sendLocationApiError", t.toString());
            }
        });
    }

    private static HashMap<String, String> getStepParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("Mileage", jobPackage.getKm());
        params.put("Fuel Gauge", jobPackage.getFuel());
        params.put("Next Date CT", jobPackage.getDate());
        params.put("Slope Reports", jobPackage.getReport());
        params.put("Comment", jobPackage.getComment());
        params.put("image", jobPackage.getImage());
        return params;
    }

    public static void stepsApi(String jobID, final int stepNo, final OnStepCompleteListener listener) {

        String url = baseURL + "rdv/v1/notify/" + jobID + "/" + stepNo + "/" + User.lat + "/" + User.lng;
        Call<ResponseBody> call = getAPI().steps(AppPersistence.token, url);
        switch (stepNo) {
            case 3:
                url = baseURL + "rdv/v1/beforeapp/" + jobID + "/" + jobPackage.getKm() + "/" + jobPackage.getFuel() + "/"
                        + jobPackage.getDate() + "/" + jobPackage.getReport() + "/" + jobPackage.getComment() + "/" + jobPackage.getTimeStamp();
                url = baseURL + "rdv/v1/beforeapp/";
                call = getAPI().stepsImage(AppPersistence.token, url, getStepParams());
                break;
            case 4:
                url = baseURL + "rdv/v1/afterapp/" + jobID + "/" + jobPackage.getKm() + "/" + jobPackage.getFuel() + "/"
                        + jobPackage.getDate() + "/" + jobPackage.getReport() + "/" + jobPackage.getComment() + "/" + jobPackage.getTimeStamp();
                url = baseURL + "rdv/v1/afterapp/";
                call = getAPI().stepsImage(AppPersistence.token, url, getStepParams());
                break;

        }

        Log.i("stepsApiURL", url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.i("StepsAPI", response + "");
                if (response.isSuccessful()) {

                    listener.OnStepCompleted(stepNo);
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                listener.OnStepFailed(stepNo, t.toString());
            }
        });


    }

    public static void login(final Context c, Auth auth, final OnLoginListener l) {

        getAPI().login(auth).enqueue(new Callback<Auth>() {
            @Override
            public void onResponse(@NonNull Call<Auth> call, @NonNull Response<Auth> response) {
                Auth a = response.body();
                Log.i("LoginAPI", new Gson().toJson(a));
                if (response.isSuccessful()) {
                    assert a != null;
                    l.OnLoginSuccess(a.getUser());
                } else {
                    // a = parseError(response);

                    l.OnLoginFail(c.getString(R.string.error));
                }


            }


            @Override
            public void onFailure(@NonNull Call<Auth> call, @NonNull Throwable t) {
                Log.e("LoginError", t.toString());
                l.OnLoginFail(t.toString());
            }
        });
    }

    private static Auth parseError(Response<?> response) {
        Converter<ResponseBody, Auth> converter =
                retrofit
                        .responseBodyConverter(Auth.class, new Annotation[0]);

        Auth error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new Auth(null, null);
        }

        return error;
    }

}
