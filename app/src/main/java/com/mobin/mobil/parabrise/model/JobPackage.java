package com.mobin.mobil.parabrise.model;

/**
 * Created by macbook on 27/11/2017.
 */

public class JobPackage {


    private String fuel, km, report, date, comment, image;
    private long timeStamp;

    public JobPackage(String fuel, String km, String report, String date, String comment, long timeStamp, String image) {
        this.fuel = fuel;
        this.km = km;
        this.report = report;
        this.date = date;
        this.comment = comment;
        this.timeStamp = timeStamp;
        this.image = image;

    }

    public String getImage() {
        return image;
    }

    public String getReport() {
        return report;
    }

    public String getDate() {
        return date;
    }

    public String getComment() {
        return comment;
    }

    public String getKm() {
        return km;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getFuel() {
        return fuel;
    }
}
